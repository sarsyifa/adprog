package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {

    @GetMapping("/profile")
    public String profile(@RequestParam(name = "visitor", required = true)
                                  String name, Model model) {
        model.addAttribute("visitor", name);
        
        String pageTitle = name.equals("") ? "This is my CV" : name
                + ", " + "I hope you're interested to hire me";
        model.addAttribute("pageTitle", pageTitle);
        return "profile";
    }

}
