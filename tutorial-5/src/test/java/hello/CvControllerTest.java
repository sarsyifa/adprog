package hello;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CvController.class)

public class CvControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testWithoutParam() throws Exception {
        mockMvc.perform(get("/profile").param("visitor", "")).andExpect(content().string(containsString("This is my CV")));
    }

    @Test(expected = AssertionError.class)
    public void testWithoutParamNeg() throws Exception {
        mockMvc.perform(get("/profile").param("visitor", ""))
                .andExpect(content().string(containsString("Syifa, I hope you&#39;re interested to hire me")));
    }

    @Test
    public void testWithParam() throws Exception {
        mockMvc.perform(get("/profile").param("visitor", "Syifa"))
                .andExpect(content().string(containsString("Syifa, I hope you&#39;re interested to hire me")));
    }

    @Test(expected = AssertionError.class)
    public void testWithParamNeg() throws Exception {
        mockMvc.perform(get("/profile").param("visitor", "Syifa"))
                .andExpect(content().string(containsString("This is my CV")));
    }

}
