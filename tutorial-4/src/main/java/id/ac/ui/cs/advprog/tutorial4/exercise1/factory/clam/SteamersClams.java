package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class SteamersClams implements Clams {

    public String toString() {
        return "Steamers Clams from Atlantic Ocean";
    }
}
