package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class SmokeyBbqSauce implements Sauce {
    public String toString() {
        return "Old Smokey BBQ Sauce";
    }
}
