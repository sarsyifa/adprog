package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SmokeyBbqSauceTest {
    private Sauce sauce;

    @Before
    public void setUp() {
        sauce = new SmokeyBbqSauce();
    }

    @Test
    public void testSmokeyBbqSauceToStringMethod() {
        assertEquals(sauce.toString(), "Old Smokey BBQ Sauce");
    }
}
