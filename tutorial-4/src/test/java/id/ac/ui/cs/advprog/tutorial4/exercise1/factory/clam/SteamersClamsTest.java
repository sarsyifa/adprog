package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SteamersClamsTest {
    private Clams clams;

    @Before
    public void setUp() {
        clams = new SteamersClams();
    }

    @Test
    public void testSteamersClamsToStringMethod() {
        assertEquals(clams.toString(), "Steamers Clams from Atlantic Ocean");
    }

}
