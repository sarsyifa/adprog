package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ProvoloneCheeseTest {
    private Cheese cheese;

    @Before
    public void setUp() throws Exception {
        cheese = new ProvoloneCheese();
    }

    @Test
    public void testProvoloneCheeseToStringMethod() throws Exception {
        assertEquals(cheese.toString(), "Provolone Cheese");
    }
}
