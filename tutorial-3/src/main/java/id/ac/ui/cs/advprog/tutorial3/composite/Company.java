package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        //TODO Implement
        this.employeesList.add(employees);
    }

    public double getNetSalaries() {
        //TODO Implement
        double netSalaries = 0;
        for (Employees e : employeesList) {
            netSalaries += e.getSalary();
        }
        return netSalaries;
    }

    public List<Employees> getAllEmployees() {
        //TODO Implement
        return employeesList;
    }

    public static void main(String[] args) {
        Company ctgroup = new Company();
    
        ctgroup.addEmployee(new Ceo("S.A.R Syifa", 200000.00));
        ctgroup.addEmployee(new Cto("Shania", 100000.00));
    
        ctgroup.addEmployee(new BackendProgrammer("Rasika", 20000.00));
        ctgroup.addEmployee(new FrontendProgrammer("Ailsa", 30000.00));
        ctgroup.addEmployee(new NetworkExpert("Naufaldi", 20000.00));
        ctgroup.addEmployee(new SecurityExpert("Fandy", 70000.00));
        ctgroup.addEmployee(new UiUxDesigner("Igor", 90000.00));
        
        System.out.println(ctgroup.getAllEmployees());
        System.out.println(ctgroup.getNetSalaries());
    }
}
