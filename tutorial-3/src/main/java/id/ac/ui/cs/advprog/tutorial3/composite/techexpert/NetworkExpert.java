package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    private String name;
    private double salary;
    private static final double MINIMUM_SALARY = 50000.00;
    private String role;

    public NetworkExpert(String name, double salary) {
        this.name = name;
        if (salary < MINIMUM_SALARY) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
        this.role = "Network Expert";

    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }
}
