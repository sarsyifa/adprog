package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;
import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class DecoratorMain {
    public static void main(String[] args) {
        Food krabbyPatty = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.println(krabbyPatty.getDescription() + " " + krabbyPatty.cost());

        krabbyPatty = FillingDecorator.BEEF_MEAT.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription() + " " + krabbyPatty.cost());

        krabbyPatty = FillingDecorator.CHEESE.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription() + " " + krabbyPatty.cost());

        krabbyPatty = FillingDecorator.CHILI_SAUCE.addFillingToBread(krabbyPatty);
        System.out.println(krabbyPatty.getDescription() + " " + krabbyPatty.cost());

        Food vegSandwich = BreadProducer.NO_CRUST_SANDWICH.createBreadToBeFilled();
        System.out.println(vegSandwich.getDescription() + " " + vegSandwich.cost());

        vegSandwich = FillingDecorator.LETTUCE.addFillingToBread(vegSandwich);
        System.out.println(vegSandwich.getDescription() + " " + vegSandwich.cost());

        vegSandwich = FillingDecorator.CHEESE.addFillingToBread(vegSandwich);
        System.out.println(vegSandwich.getDescription() + " " + vegSandwich.cost());
    

    }
}