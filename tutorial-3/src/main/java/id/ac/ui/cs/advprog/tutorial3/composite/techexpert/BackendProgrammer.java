package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {
    //TODO Implement
    private String name;
    private double salary;
    private static final double MINIMUM_SALARY = 20000.00;
    private String role;

    public BackendProgrammer(String name, double salary) {
        //TODO Implement
        this.name = name;
        if (salary < MINIMUM_SALARY) {
            throw new IllegalArgumentException();
        } else {
            this.salary = salary;
        }
        this.role = "Back End Programmer";

    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getRole() {
        return role;
    }
}
