package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;

public class WeatherData extends Observable {

    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData(){}

    public void measurementsChanged() {
        setChanged(); //kalo ada data yg berubah dia ngeubah data flag ini ada yg ke update trs ke triggered notifyObserver
        notifyObservers();//dia notify semua orang yg butuhdata dia bakal manggil method update semua yg ada di observer
    }

    public void setMeasurements(float temperature, float humidity,
                                float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public float getTemperature() {
        // TODO Complete me!
        return temperature;
    }

    public void setTemperature(float temperature) {
        // TODO Complete me!
        this.temperature = temperature;
    }

    public float getHumidity() {
        // TODO Complete me!
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getPressure() {
        // TODO Complete me!
        return pressure;
    }

    public void setPressure(float pressure) {
        // TODO Complete me!
        this.pressure = pressure;
    }
}
